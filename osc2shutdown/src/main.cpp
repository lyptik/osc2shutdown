#include "ofMain.h"
#include "ofApp.h"
#include "ofAppNoWindow.h"

std::string resourcePath;

//========================================================================
int main( ){
    
#ifdef __APPLE__
    // This makes relative paths work in C++ in Xcode by changing directory to the Resources folder inside the .app bundle
    // ----------------------------------------------------------------------------
    char path[2048];
    CFBundleRef mainBundle = CFBundleGetMainBundle();
    CFURLRef resourcesURL = CFBundleCopyResourcesDirectoryURL(mainBundle);
    if (!CFURLGetFileSystemRepresentation(resourcesURL, TRUE, (UInt8 *)path, 2048))
    {
        std::cerr << "Error : couldn't get bundle path resources" << std::endl;
    }
    CFRelease(resourcesURL);
    
    
    //int result = chdir(path); // do not move path because we are still using openframeworks stuff
    resourcePath = ofToString(path);
    resourcePath.append("/");
    std::cout << "Current Path: " << resourcePath << std::endl;
    ofSetDataPathRoot(resourcePath); // openframeworks way to append string
#else
    resourcePath = "";
#endif
    
    ofSetupOpenGL(640,70, OF_WINDOW);			// <-------- setup the GL context
    /* Replace by this if you need headless mode
    ofAppNoWindow w;
	ofSetupOpenGL(&w,640,480, OF_WINDOW);			// <-------- setup the GL context
     */
    
	// this kicks off the running of my app
	// can be OF_WINDOW or OF_FULLSCREEN
	// pass in width and height too:
	ofRunApp(new ofApp());
}
