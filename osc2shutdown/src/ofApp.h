#pragma once

#include "ofMain.h"
#include "ofxOsc.h"
#include "ofxXmlSettings.h"

class ofApp : public ofBaseApp {

    public:

		void setup();
		void update();
		void draw();
        void keyReleased(int key);
    
    private:
    
        void loadSettings();
        //void shutdownComputer(); // Deprecated
        //void restartComputer(); // Deprecated
        void launchClosingScript(string);
    
        ofxXmlSettings m_xml;
    
        // Network
		ofxOscReceiver receiver;
    
        // Last msg info
        int m_iListeningPort;
        string m_sMsg;
        string m_sTimeStamp;
        bool hasBoundToPort;
};
