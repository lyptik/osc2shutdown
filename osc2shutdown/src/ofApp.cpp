#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    
    ofSetFrameRate(10);
    
    ofSetWindowTitle("osc2Shutdown");
    
    loadSettings();
    
    hasBoundToPort = true; // init
    
    try {
        receiver.setup(m_iListeningPort);
    } catch (std::exception&e) {
        std::cerr << "Error : " << e.what() << endl;
        ofLogWarning("Could not bind to port " + ofToString(m_iListeningPort) + " !");
        hasBoundToPort = false;
    }

    // listen on the given port
    ofLogVerbose("Listening for osc messages on port " + ofToString(m_iListeningPort));
    
	ofBackground(80,80,80);
    
    m_sMsg = "";
    m_sTimeStamp = "";
}

//--------------------------------------------------------------
void ofApp::loadSettings(){

    string sSettings = "settings.xml";
    
    // Load the settings
     if( m_xml.loadFile(sSettings) ){
         ofLogVerbose(sSettings + "loaded !");
         
         m_iListeningPort = m_xml.getValue("osc:port", 9000);

     }else{
         ofLogWarning("Failed to load " + sSettings);
         ofLogWarning("Creating " + sSettings + " with defaults values");
         
         m_xml.setValue("osc:port", 9000); // write value
         m_iListeningPort = m_xml.getValue("osc:port", 9000); // load value

         m_xml.save(sSettings);
     }
}

//--------------------------------------------------------------
void ofApp::update(){

	// check for waiting messages
	while(receiver.hasWaitingMessages()){
        
		// get the next message
		ofxOscMessage m;
		receiver.getNextMessage(m);
        m_sTimeStamp = ofGetTimestampString("%Y/%m/%d at %H:%M:%S");
        
        m_sMsg = m.getAddress();
        
        if(m_sMsg == "/shutdown"){
            
            launchClosingScript("shutdown");
            
        } else if(m_sMsg == "/restart"){
            
            launchClosingScript("restart");
            
        }else{
            
            // unrecognized message -> UI display
            m_sMsg = m.getAddress();
            
            for(int i = 0; i < m.getNumArgs(); i++){

                // Get the argument type
                m_sMsg += " ";
                m_sMsg += m.getArgTypeName(i);
                m_sMsg += ":";
                
                // Display the argument - make sure we get the right type
                if(m.getArgType(i) == OFXOSC_TYPE_INT32){
                    m_sMsg += ofToString(m.getArgAsInt32(i));
                }
                else if(m.getArgType(i) == OFXOSC_TYPE_FLOAT){
                    m_sMsg += ofToString(m.getArgAsFloat(i));
                }
                else if(m.getArgType(i) == OFXOSC_TYPE_STRING){
                    m_sMsg += m.getArgAsString(i);
                }
                else{
                    m_sMsg += "unknown";
                }
            }
        }
	}
}

//--------------------------------------------------------------
void ofApp::draw(){
    
    stringstream ss;
    if(hasBoundToPort) {
        ss << "Listening to OSC /shutdown and /restart on port " << ofToString(m_iListeningPort) << endl
        << "Press 'S' to shutdown and 'R' to restart";
    } else {
        ss << "/!\\ Could not bind to port " << ofToString(m_iListeningPort) << " ! You can change port in settings.xml" << endl
        << "Press 'S' to shutdown and 'R' to restart";
    }
	ofDrawBitmapString(ss.str(), 10, 20);
    
    if (m_sMsg != ""){
        stringstream msg;
        msg << "Last message received " << m_sTimeStamp << " : " << m_sMsg;
        ofDrawBitmapString(msg.str(), 10, 50);
    }
}

//--------------------------------------------------------------
void ofApp::launchClosingScript(string a_inputArg){
    
    // Input args can be shutdown or restart

    ofLogNotice() << "Launching closing script..." << endl;
    
    #if defined(__APPLE__) || defined(__linux__)
    // launch Closing script
    string cmd = "nohup $PWD/closingScript.sh " + a_inputArg + " </dev/null >/dev/null 2>&1 &";
    ofLogNotice() << cmd << endl;
    system(cmd.c_str());
    #endif
    
    #ifdef _WIN32
    // TODO : give args to the script or trigger one of the other script
    string cmd = "cd data & start closingScript_WIN32.bat";
    system(cmd.c_str());
    #endif
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

    // We use capitol letters and keyReleased to protect from unexpected triggering
    
    switch(key){
            
        case 'S':
            launchClosingScript("shutdown");
            break;
            
        case 'R':
            launchClosingScript("restart");
            break;
            
        default:
            break;
    }
}

/*

//------------------------DEPRECATED-----------------------------
void ofApp::shutdownComputer(){

    ofLogNotice() << "Shutting down machine..." << endl;
    
    #if defined(__APPLE__) || defined(__linux__)
    string cmd = "echo " + m_sPwd + " | sudo -S shutdown -h now"; // shuts down now
    ofLogNotice() << cmd << endl;
    system(cmd.c_str());
    #endif
    
    #ifdef _WIN32
    string cmd = "shutdown -f -s"; // Force running applicatin to close and shuts down now
    system(cmd.c_str());
    #endif
    
}

//------------------------DEPRECATED-----------------------------
void ofApp::restartComputer(){
    
    ofLogNotice() << "Shutting down machine..." << endl;
    
    #if defined(__APPLE__) || defined(__linux__)
    string cmd = "echo " + m_sPwd + " | sudo -S shutdown -r now";
    ofLogNotice() << cmd << endl;
    system(cmd.c_str());
    #endif
    
    #ifdef _WIN32
    string cmd = "shutdown -f -r";
    system(cmd.c_str());
    #endif
    
}

*/
