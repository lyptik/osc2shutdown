#!/bin/sh

# Closing script launched before shutdown

# This script (will be copied) or should be in $YOUR_APP_NAME/Contents/Resources

MY_SUDO_PASSWORD=dumbpassword
LOG_FILE=closingScript.log
touch $LOG_FILE


case `uname` in

  Darwin)
	echo "OSX detected, lauching closingScript_OSX.scpt with input $1" > $LOG_FILE
	osascript closingScript_OSX.scpt $1
	;;

  Linux)
	echo "Linux detected, executing script with input $1" > $LOG_FILE
	pkill osc2Shutdown
	sleep 1
	if [ "$1" == "shutdown" ]; then
		echo $MY_SUDO_PASSWORD | sudo -S shutdown -h now
	else
		echo $MY_SUDO_PASSWORD | sudo -S shutdown -r now
	fi
	;;
esac
