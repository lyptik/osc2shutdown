#osc2shutdown

## Concept

The software listens to the OSC packet /shutdown or /restart and then launch a script with argument shutdown or restart

The script close the application, optionnaly close other applications then shutdown or restart the computer.

Implement your correct closing workflow in the script so the machine can restart properly

For launching script, check an example in the scripts folder

## Features

	- You can change the osc listening port into the xml file (into the .app)
	- You can add your own commands in the script
	- The app trigger the closing script from OSC or keyboard

## Platform

This has been tested so far with OFX 0.9 on mac OSX 10.10 and needs testing to be fully working for windows and linux as well

#### OSX

Scripts are in osc2Shutdown.app/contents/resources

closingScript.sh bash script is triggered by the app which itself triggers closingScript_OSX.scpt Apple Script

Apple script is used to be able to shutdown the computer without being a super user.

shutdown and restart argument are passed from the bash script to the Apple Script

Install this application in your Application folder

#### Linux

closingScript.sh bash script is triggered by the app (with input args shutdown or restart)
Sudo password must be in the script in order to shutdown the machine (or your user should belong to the correct group)
(This is of course very bad practice to put a sudo password in a plain text file)

#### Windows

closingScript_WIN32.bat script is triggered, still need some extra love and testing to be fully working

## Automation

### Launch

- Install the app osc2shutdown.app in your /Applications folder
- Launch it
- Right clic on the icon on the dock, then Options, then "Open with session"
	-> This will launch the application at startup
- Check that your session is in automatic login (Preferences -> Users and groups)

### Shutdown

- Install the app ShutdownWithCalendar_OSX.app in your /Applications folder
- Create an event in OSX Calendar app
- Add an event
- Add an alert and choose Personalize then "open the file"
- Point to the app ShutdownWithCalendar_OSX.app 
